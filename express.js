import express from 'express'
import https from 'https'
import fs from 'fs'

const app = express();
const PORT = process.env.PORT || 3001;

app.get('/express', (req, res) => {
  res.send('Hello from proxy Express.js');
});

const options = {
  cert: fs.readFileSync('/etc/ssl/certs/${DOCKER_SSL_CERTIFICATEFILE}'),
  key: fs.readFileSync('/etc/ssl/private/${DOCKER_SSL_PRIVATEKEYFILE}')
};

const server = https.createServer(options, app);

server.listen(PORT, () => {
  console.log(`Express.js server running on https://localhost:${PORT}/`);
});
