# Web動作環境

FROM debian:12-slim AS stage01
RUN apt update && apt upgrade -y && \
    apt install -y \
    curl jq nano

# ロケールを日本に設定
FROM stage01 AS stage02
RUN apt install -y \
    locales tzdata
RUN sed -i -E 's/^#\s*(ja_JP.UTF-8)/\1/' /etc/locale.gen && \
    locale-gen && \
    update-locale LANG=ja_JP.UTF-8 && \
    echo 'export LC_ALL=ja_JP.UTF-8' >> /root/.bashrc

# 鍵ファイル
FROM stage02 AS stage03
ARG DOCKER_SSL_PRIVATEKEYFILE=company.key
ARG DOCKER_SSL_CERTIFICATEFILE=company.crt
ARG DOCKER_SSL_COMPANYNAME=company
RUN apt install -y \
    ssl-cert openssl
#COPY ${DOCKER_SSL_CERTIFICATEFILE} /etc/ssl/certs/
#COPY ${DOCKER_SSL_PRIVATEKEYFILE} /etc/ssl/private/
# 仮鍵ファイル作成（メソッド１）
RUN make-ssl-cert generate-default-snakeoil --force-overwrite
# 仮鍵ファイル作成（メソッド２）
#RUN openssl req -new -newkey ed25519 -x509 -nodes -out /etc/ssl/certs/${DOCKER_SSL_CERTIFICATEFILE} -keyout /etc/ssl/private/${DOCKER_SSL_PRIVATEKEYFILE} -subj "/C=US/ST=State/L=City/O=Organization/CN=${DOCKER_SSL_COMPANYNAME}" -days 365

# SSH
FROM stage03 AS stage04
RUN apt install -y \
    openssh-server
RUN cp /etc/ssl/certs/${DOCKER_SSL_CERTIFICATEFILE} /root/.ssh/authorized_keys && \
    chmod 600 /root/.ssh/authorized_keys && \
    chmod 700 /root/.ssh && \
    sed -i -E 's/(^#?UsePAM\s.*\$)/UsePAM yes/' /etc/ssh/sshd_config

# Apache
FROM stage04 AS stage05
ARG DOCKER_HTTPD_SERVERDOMEIN=localhost
RUN apt install -y \
    apache2
RUN a2enmod rewrite && \
    sed -i -E 's/(Directory \/var\/www\/)/\1html/' /etc/apache2/apache2.conf && \
    echo "ServerName ${DOCKER_HTTPD_SERVERDOMEIN}" >> /etc/apache2/apache2.conf
# SSL設定
RUN a2enmod ssl
RUN sed -i "/A self-signed (snakeoil)/,/Server Certificate Chain/ s/ssl-cert-snakeoil.pem/${DOCKER_SSL_CERTIFICATEFILE}/" /etc/apache2/sites-available/default-ssl.conf && \
    sed -i "/A self-signed (snakeoil)/,/Server Certificate Chain/ s/ssl-cert-snakeoil.key/${DOCKER_SSL_PRIVATEKEYFILE}/" /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E "s/(<VirtualHost \*:80>)/\1\n        # SSLにリダイレクト\n        Redirect permanent \/ https:\/\/${DOCKER_HTTPD_SERVERDOMEIN}\/\n/" /etc/apache2/sites-available/000-default.conf && \
    a2ensite default-ssl.conf

# MariaDB
FROM stage05 AS stage06
ARG DOCKER_MARIADB_ROOTPASSWORD=rootpassword
ARG DOCKER_MARIADB_USERNAME=user
ARG DOCKER_MARIADB_USERPASSWORD=userpassword
RUN apt install -y \
    mariadb-server expect && \
    service mariadb start && \
    export MariaDBRootPasswd=${DOCKER_MARIADB_ROOTPASSWORD} && \
    expect -c ' \
    set timeout 1; \
    spawn mysql_secure_installation; \
    expect "Enter current password for root (enter for none): "; \
    send -- "\n"; \
    expect "Set root password?"; \
    send -- "'Y'\n"; \
    expect "New password:"; \
    send -- "'"${MysqlRootPasswd}"'\n"; \
    expect "Re-enter new password:"; \
    send -- "'"${MysqlRootPasswd}"'\n"; \
    expect "Remove anonymous users?"; \
    send "Y\n"; \
    expect "Disallow root login remotely?"; \
    send "Y\n"; \
    expect "Remove test database and access to it?"; \
    send "Y\n"; \
    expect "Reload privilege tables now?"; \
    send "Y\n"; \
    interact;' && \
    export MariaDBUserName=${DOCKER_MARIADB_USERNAME} && \
    export MariaDBUserPassword=${DOCKER_MARIADB_USERPASSWORD} && \
    expect -c ' \
    set timeout 1; \
    spawn mysql -u root -p; \
    expect "Enter password:"; \
    send -- "'"${MysqlRootPasswd}"'\n"; \
    expect "MariaDB \[\(none\)\]>"; \
    send -- "CREATE USER '\''${MariaDBUserName}'\''@'\''localhost'\'' IDENTIFIED BY '\''${MariaDBUserPassword}'\'';\n" \
    expect "MariaDB \[\(none\)\]>"; \
    send -- "FLUSH PRIVILEGES;\n" \
    expect "MariaDB \[\(none\)\]>"; \
    send -- "exit;\n" \
    interact;'

# PHP(FastCGI)
FROM stage06 AS stage07
ARG DOCKER_PHP_VER=8.2
ARG DOCKER_MYPHPADMIN_PASSWORD=phpmyadmin
RUN debconf-set-selections phpmyadmin/mysql/admin-user multiselect root && \
    debconf-set-selections phpmyadmin/dbconfig-install boolean true && \
    debconf-set-selections phpmyadmin/reconfigure-webserver multiselect apache2 && \
    debconf-set-selections phpmyadmin/app-password-confirm password ${DOCKER_MYPHPADMIN_PASSWORD} && \
    DEBIAN_FRONTEND=noninteractive apt install -y \
    libapache2-mod-fcgid \
    php php-cli php-http php-phar php-mysql php-mbstring php-gd php-curl php-zip php-fpm phpmyadmin
RUN a2enmod fcgid && \
    a2enconf php${DOCKER_PHP_VER}-fpm
RUN sed -i -E '/SSL Engine Switch/,/A self-signed \(snakeoil\)/ s/(^\s+)(SSLEngine on)/\1\2\n\1SSLProxyEngine on/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E '/<FilesMatch "\\\.\(/,/<\/FilesMatch>/ s/(^\s+SSLOptions)/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E 's/(^\s+<FilesMatch "\\\.\()/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E 's/(^\s+<\/FilesMatch>)/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E '/<Directory \/usr\/lib\/cgi-bin>/,/<\/Directory>/ s/(^\s+SSLOptions)/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E 's/(^\s+<Directory \/usr\/lib\/cgi-bin>)/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
    sed -i -E 's/(^\s+<\/Directory>)/#\1/' /etc/apache2/sites-available/default-ssl.conf && \
# phpinfo.php
COPY phpinfo.php /var/www/html/
RUN chmod +x /var/www/html/phpinfo.php

# Python(WSGI)
FROM stage07 AS stage08
RUN apt install -y \
    libapache2-mod-wsgi-py3 \
    python3 python3-poetry
# wsgi.py
RUN sed -i -E 's/(<\/VirtualHost>)/        # Python(WSGI)\n        <Directory \/var\/www\/html\/wsgi>\n                Require all granted\n        <\/Directory>\n\n        WSGIScriptAlias \/wsgi \/var\/www\/html\/wsgi\/wsgi.py\n\n\1/' /etc/apache2/sites-available/default-ssl.conf
RUN a2enmod wsgi
COPY wsgi.py ./
RUN mkdir /var/www/html/wsgi && \
    mv wsgi.py /var/www/html/wsgi/ && \
    chmod +x /var/www/html/wsgi/wsgi.py && \
    cd /var/www/html/wsgi && \
    poetry init -n

# NVM・Node.js・npm
FROM stage08 AS stage09
ARG DOCKER_NODE_VER=20.10.0
ENV NVM_DIR /root/.nvm
RUN mkdir -p ${NVM_DIR} && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash && \
    . $NVM_DIR/nvm.sh && \
    nvm install ${DOCKER_NODE_VER} && \
    nvm use ${DOCKER_NODE_VER} && \
    npm install -g npm pnpm yarn npm-check-updates && \
    echo "export NVM_DIR=$NVM_DIR\n. $NVM_DIR/nvm.sh" >> /root/.bashrc
ENV NODE_PATH $NVM_DIR/v${DOCKER_NODE_VER}/lib/node_modules
ENV PATH $PATH:$NVM_DIR/versions/node/v${DOCKER_NODE_VER}/bin

# Node.js(proxy)
FROM stage09 AS stage10
RUN sed -i -E 's/(<\/VirtualHost>)/        # Node.js(proxy)\n        ProxyPass \/node https:\/\/localhost:3000\/node\n        ProxyPassReverse \/node https:\/\/localhost:3000\/node\n\n        <Directory \/var\/www\/html\/node>\n                Options FollowSymLinks\n                AllowOverride None\n                Require all granted\n        <\/Directory>\n\n\1/' /etc/apache2/sites-available/default-ssl.conf
# node.js
RUN a2enmod proxy proxy_html proxy_http
COPY node.js ./
RUN mkdir /var/www/html/node && \
    mv node.js /var/www/html/node/script.js && \
    sed -i "s/\${DOCKER_SSL_CERTIFICATEFILE}/${DOCKER_SSL_CERTIFICATEFILE}/" /var/www/html/node/script.js && \
    sed -i "s/\${DOCKER_SSL_PRIVATEKEYFILE}/${DOCKER_SSL_PRIVATEKEYFILE}/" /var/www/html/node/script.js && \
    . "$NVM_DIR/nvm.sh" && \
    cd /var/www/html/node && \
    npm init -y && \
    cat package.json | jq '.+ {"name": "node", "main": "script.js", "type": "module"}' > tmp0 && \
    mv tmp0 package.json

# Express.js(proxy)
FROM stage10 AS stage11
RUN sed -i -E 's/(<\/VirtualHost>)/        # Express.js(Node.js, proxy)\n        ProxyPass \/express https:\/\/localhost:3001\/express\n        ProxyPassReverse \/express https:\/\/localhost:3001\/express\n\n        <Directory \/var\/www\/html\/express>\n                Options FollowSymLinks\n                AllowOverride None\n                Require all granted\n        <\/Directory>\n\n\1/' /etc/apache2/sites-available/default-ssl.conf
# express.js
COPY express.js ./
RUN mkdir /var/www/html/express && \
    mv express.js /var/www/html/express/script.js && \
    sed -i "s/\${DOCKER_SSL_CERTIFICATEFILE}/${DOCKER_SSL_CERTIFICATEFILE}/" /var/www/html/express/script.js && \
    sed -i "s/\${DOCKER_SSL_PRIVATEKEYFILE}/${DOCKER_SSL_PRIVATEKEYFILE}/" /var/www/html/express/script.js && \
    . "$NVM_DIR/nvm.sh" && \
    cd /var/www/html/express && \
    npm init -y && \
    npm install express && \
    cat package.json | jq '.+ {"name": "express", "main": "script.js", "type": "module"}' > tmp0 && \
    mv tmp0 package.json

# Postfix
FROM stage11 AS stage12
ARG DOCKER_POSTFIX_SMTP=localhost
ARG DOCKER_POSTFIX_MAIL=company@localhost
ARG DOCKER_POSTFIX_PASSWORD=mailpassword
RUN apt install -y \
    postfix
# TLS設定
RUN postconf -e smtpd_tls_key_file=/etc/ssl/private/${DOCKER_SSL_PRIVATEKEYFILE} && \
    postconf -e smtpd_tls_cert_file=/etc/ssl/certs/${DOCKER_SSL_CERTIFICATEFILE} && \
    postconf -e smtpd_use_tls=yes
# メール設定
RUN echo "[${DOCKER_POSTFIX_SMTP}]:587 ${DOCKER_POSTFIX_MAIL}:${DOCKER_POSTFIX_PASSWORD}" > /etc/postfix/mail && \
    chmod 600 /etc/postfix/mail && \
    postmap /etc/postfix/mail && \
    sed -i -E "s/^(relayhost =)/\1 [${DOCKER_POSTFIX_SMTP}]:587/" /etc/postfix/main.cf && \
    echo 'smtp_sasl_auth_enable = yes' >> /etc/postfix/main.cf && \
    echo 'smtp_sasl_password_maps = hash:/etc/postfix/mail' >> /etc/postfix/main.cf && \
    echo 'smtp_sasl_security_options = noanonymous' >> /etc/postfix/main.cf && \
    echo 'smtp_sasl_tls_security_options = noanonymous' >> /etc/postfix/main.cf && \
    echo 'smtp_sasl_mechanism_filter = plain' >> /etc/postfix/main.cf && \
    sed -i -E 's/^(smtp_use_tls =).*\$/\1 yes/' /etc/postfix/main.cf

FROM stage12 AS stage13

# ポート出力
EXPOSE 22 80 443 587

# コンテナ起動時
COPY start.sh ./
RUN sed -i "s/\${DOCKER_PHP_VER}/${DOCKER_PHP_VER}/" start.sh
RUN chmod +x start.sh
CMD ["./start.sh"]
