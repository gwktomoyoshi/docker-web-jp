import https from 'https'
import fs from 'fs'

const PORT = process.env.PORT || 3000;

const options = {
  cert: fs.readFileSync('/etc/ssl/certs/${DOCKER_SSL_CERTIFICATEFILE}'),
  key: fs.readFileSync('/etc/ssl/private/${DOCKER_SSL_PRIVATEKEYFILE}')
};

const server = https.createServer(options, (req, res) => {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello from proxy Node.js');
});

server.listen(PORT, () => {
  console.log(`Node.js server running on https://localhost:${PORT}/`);
});
