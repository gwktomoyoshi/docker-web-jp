#!/usr/bin/env bash

source /root/.bashrc
service ssh start
service mariadb start
service postfix start
service php${DOCKER_PHP_VER}-fpm start
node /var/www/html/node/script.js &
node /var/www/html/express/script.js &
rm -f /var/run/apache2/apache2.pid
apache2ctl -D FOREGROUND
