# docker-web-jp

<table style="border: none;">
  <tr align="center">
    <td><img src="images/docker.svg" width="120"></td>
    <td><img src="images/debian.png" width="80"></td>
  </tr>
  <tr align="center">
    <td><img src="images/apache.svg" width="120"></td>
    <td><img src="images/mariadb.svg" width="120"></td>
  </tr>
  <tr align="center">
    <td colspan="2"><img src="images/postfix.svg" width="120"></td>
  </tr>
</table>

LAMPなWeb動作環境です。  

## 構成

- OS: Debian bookworm(12) slim
- httpd: Apache2 (SSL only)
  - FastCGI (for PHP)
  - WSGI (for Python)
  - Node.js LTS (+ Express)
- sshd: OpenSSH server
- DB: MariaDB
- sendmail: Postfix (SSL only)

## Web

- http ポート80番は、https ポート443番に強制リダイレクトします。

```
/var/www/html/
├ index.html ： Apacheトップ
├ phpinfo.php ： phpinfo(FastCGI)
├ wsgi/
│ └ wsgi.py ： python(WSGI) ※エントリポイントは/wsgi
├ node/
│ └ script.py ： Node.js(proxy) ※エントリポイントは/node
└ express/
   └ script.py ： Express.js(proxy) ※エントリポイントは/express
```

## Docker Hub

このイメージは、[Docker Hub](https://hub.docker.com/) にて、
`gwktomoyoshi/web-jp` でホストしています。
debugブランチのイメージは、`gwktomoyoshi/web-jp-debug`でホストしています。

## イメージからコンテナを作成して実行

1. イメージを pull する。

```bash
docker pull gwktomoyoshi/web-jp
```

2. my-web-jp コンテナとボリュームを作成する。  
   OpenSSH のポート22番は除外（ホストとカブる、本番は必要に応じて）

```bash
docker create -v web-jp_www:/var/www/html:rw -v web-jp_db:/var/lib/mysql:rw -p 80:80 -p 443:443 -p 587:587 --name my-web-jp gwktomoyoshi/web-jp
```

3. 起動 / 停止 / 再起動 / 削除 

```bash
docker start my-web-jp
docker stop my-web-jp
docker restart my-web-jp
docker rm my-web-jp
```

4. 起動中のコンテナ内のbashを実行 / 終了

```bash
docker exec -it my-web-jp bash
```

```bash
root@12345678abcd:/# exit
```

## イメージをビルド （イメージを開発する人向け）

1. GitLab から プロジェクトを clone する。

```bash
git clone https://gitlab.com/gwktomoyoshi/docker-web-jp.git
cd docker-web-jp
```

2. docker-compose.yml を編集する。

  - image に使いやすいイメージ名を付ける。   
  - args に必要な値を格納する。  
  （Dockerfile 内の ARG は、docker-compose.yml の値で上書きされる。）

3. イメージをビルドする。

```bash
docker-compose build --no-cache
```

## 補足

- debian:slim に、全部入れたかった  
  セキュアと原理と互換性の観点から Alpine(+busybox) は避けるべき。  
  ホビー用途の Ubuntu も避けるべき。RHEL か Debian。  
  db や sendmail でサービスを分けると、debianが増殖するのでやらない。省ハード資源で。
- 全部入りをやるためには各サーバ設定を Docker 用の手軽な設定は用いず、ガチ。  
  更にDockerのRUN時は特殊なコンテキストなので、systemcrl や init.d が使えないなど、
  裏口的な手法が必要になる。
- OpenSSHのポート22番は動かしがたく、ホストもサービスが起動してるはず。

## 参考

- [MariaDBの初期設定を自動化するスクリプトを作ってみた](https://sal-blog.com/mariadb%E3%81%AE%E5%88%9D%E6%9C%9F%E8%A8%AD%E5%AE%9A%E3%82%92%E8%87%AA%E5%8B%95%E5%8C%96%E3%81%99%E3%82%8B%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%97%E3%83%88%E3%82%92%E4%BD%9C%E3%81%A3%E3%81%A6%E3%81%BF/)
